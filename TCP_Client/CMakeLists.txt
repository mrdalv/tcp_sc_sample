cmake_minimum_required(VERSION 3.0.0)
project(TCP_CLIENT VERSION 0.1.0)

include(CTest)
enable_testing()

set(PROJECT_SOURCES
        client.cpp
)

add_executable(TCP_CLIENT ${PROJECT_SOURCES})
target_link_libraries(TCP_CLIENT -lws2_32)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})

include(CPack)
