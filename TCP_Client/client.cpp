#ifndef _WIN32
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdexcept>
#else
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include <cstdlib>
#include <cstring>
#include <iostream>

#define PORT 8001
#define ADDRESS "127.0.0.1"
#define BUFFER_SIZE 512

#ifndef _WIN32
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#endif

/* Эта функция указывает версию WinSock и настраивает "behind the scenes
stuff",
 * которые нужны для использования сокетов приложению (если Windows), в
 * противном случае просто возвращает true, так как на других ОС на этом
 этапе
 * настраивать ничего не нужно*/
bool initWSA() {
#ifdef _WIN32
    WSADATA wsaData;
    return WSAStartup(MAKEWORD(2, 2), &wsaData) == NO_ERROR;
#else
    return true;
#endif
}

/* Эта функция выполняет возврат кода ошибки*/
int getErrorCode(int errorCodeOtherOS) {
#ifdef _WIN32
    return WSAGetLastError();
#else
    return errorCodeOtherOS;
#endif
}

/* Следующая функция в случае с Windows закрывает указанный сокет и удаляет
 * структуры, инициализированные WSAStartup. Другие ОС просто закрывают
 сокет*/
int closeSocket(int tcpSocketClient) {
#ifdef _WIN32
    int iCloseSocket = closesocket(tcpSocketClient);
    if (iCloseSocket == SOCKET_ERROR) {
        std::cerr << "Socket closing failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return SOCKET_ERROR;
    } else {
        std::cout << "Socket closing success\n";

        int iWSACleanup = WSACleanup();
        if (iWSACleanup == SOCKET_ERROR) {
            std::cerr << "WSA cleanup failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            return SOCKET_ERROR;
        } else {
            std::cout << "WSA cleanup success\n";
            return 0;
        }
    }
#else
    int iClose = close(tcpSocketClient);
    if (iClose == SOCKET_ERROR) {
        std::cerr << "Socket closing failed\n";
        return SOCKET_ERROR;
    } else {
        return 0;
    }
#endif
}

/*Эта программа выступает в качестве клиента, который отправляет по TCP
 * протоколу сообщения на сервер и получает эхо*/
int main(int argc, const char** argv) {
    char buffer[BUFFER_SIZE];
    std::string message;

    if (!initWSA()) {
        std::cerr << "WSAStartup failed\n";
        return 1;
    }

    addrinfo hints;
    addrinfo* servinfo;

    // Убедиться, что структура пуста
    memset(&hints, 0, sizeof(hints));
    // IPv4
    hints.ai_family = AF_INET;
    // TCP stream sockets
    hints.ai_socktype = SOCK_STREAM;
    // Получение для servinfo связанный список с одной или более структур (в
    // дальнейшем нужно будет перебирать и биндить через цикл, но не в этом
    // примере)
    int status = getaddrinfo(nullptr, "8001", &hints, &servinfo);
    if (status != 0) {
        std::cerr << "Get address info error & Error code -> "
                  << gai_strerror(status) << std::endl;
    }

    // Создание дескриптора сокета TCP
    int tcpSocketClient = socket(servinfo->ai_family, servinfo->ai_socktype,
                                 servinfo->ai_protocol);
    if (tcpSocketClient == INVALID_SOCKET) {
        std::cerr << "Socket creation error & Error Code -> "
                  << getErrorCode(INVALID_SOCKET) << std::endl;
        return 1;
    }

    // Выполнения подключения к удаленному хосту (серверу)
    int iConnect =
        connect(tcpSocketClient, servinfo->ai_addr, servinfo->ai_addrlen);
    if (iConnect == SOCKET_ERROR) {
        std::cerr << "Connection failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return 1;
    }

    while (true) {
        std::cout << "Input the message - ";
        std::getline(std::cin, message);

        strcpy(buffer, message.c_str());

        // Отправляем сообщение на сервер
        int iSend = send(tcpSocketClient, buffer, BUFFER_SIZE, 0);
        if (message == "\\c connection") {
            std::cout << "Connection closed" << std::endl;
            break;
        } else if (iSend == SOCKET_ERROR) {
            std::cerr << "Sending failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            continue;
        }

        // Пользователь ввёл сообщение о разрыве соединения
        if (message == "\\c connection") {
            std::cout << "Connection closed" << std::endl;
            break;
        }

        // Получаем сообщения от сервера
        int iReceiveFrom = recv(tcpSocketClient, buffer, BUFFER_SIZE, 0);
        if (iReceiveFrom == SOCKET_ERROR) {
            std::cerr << "Receiving failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            return 1;
        }
        std::cout << "Reply - " << buffer << std::endl;
    }

    int iCloseSocket = closeSocket(tcpSocketClient);
    if (iCloseSocket == SOCKET_ERROR) {
        return 1;
    }
}