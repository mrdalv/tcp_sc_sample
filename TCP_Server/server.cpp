#ifndef _WIN32
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdexcept>
#else
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include <cstdlib>
#include <cstring>
#include <iostream>

#define BUFFER_SIZE 512

#ifndef _WIN32
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#endif

/* Эта функция указывает версию WinSock и настраивает "behind the scenes stuff",
 * которые нужны для использования сокетов приложению (если Windows), в
 * противном случае просто возвращает true, так как на других ОС на этом этапе
 * настраивать ничего не нужно*/
bool initWSA() {
#ifdef _WIN32
    WSADATA wsaData;
    return WSAStartup(MAKEWORD(2, 2), &wsaData) == NO_ERROR;
#else
    return true;
#endif
}

/* Эта функция выполняет возврат кода ошибки*/
int getErrorCode(int errorCodeOtherOS) {
#ifdef _WIN32
    return WSAGetLastError();
#else
    return errorCodeOtherOS;
#endif
}

/* Следующая функция в случае с Windows закрывает указанный сокет и удаляет
 * структуры, инициализированные WSAStartup. Другие ОС просто закрывают сокет*/
int closeSocket(int tcpSocketServer) {
#ifdef _WIN32
    int iCloseSocket = closesocket(tcpSocketServer);
    if (iCloseSocket == SOCKET_ERROR) {
        std::cerr << "Socket closing failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return SOCKET_ERROR;
    } else {
        std::cout << "Socket closing success\n";

        int iWSACleanup = WSACleanup();
        if (iWSACleanup == SOCKET_ERROR) {
            std::cerr << "WSA cleanup failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            return SOCKET_ERROR;
        } else {
            std::cout << "WSA cleanup success\n";
            return 0;
        }
    }
#else
    int iClose = close(tcpSocketServer);
    if (iClose == SOCKET_ERROR) {
        std::cerr << "Socket closing failed\n";
        return SOCKET_ERROR;
    } else {
        return 0;
    }
#endif
}

/* Эта программа выступает в качестве сервера, который принимает по TCP
 * протоколу сообщения от клиента и отправляет эхо*/
int main() {
    char buffer[BUFFER_SIZE];

    if (!initWSA()) {
        std::cerr << "WSAStartup failed\n";
        return 1;
    }

    addrinfo hints;
    addrinfo *servinfo;

    // Убедиться, что структура пуста
    memset(&hints, 0, sizeof(hints));
    // IPv4
    hints.ai_family = AF_INET;
    // TCP stream sockets
    hints.ai_socktype = SOCK_STREAM;
    // Заполнить IPз-адрес за меня
    hints.ai_flags = AI_PASSIVE;

    // Получение для servinfo связанный список с одной или более структур
    int status = getaddrinfo(nullptr, "8001", &hints, &servinfo);
    if (status != 0) {
        std::cerr << "Get address info error & Error code -> "
                  << gai_strerror(status) << std::endl;
    }

    // Создание дискриптора сокета TCP
    int tcpSocketServer = socket(servinfo->ai_family, servinfo->ai_socktype,
                                 servinfo->ai_protocol);
    if (tcpSocketServer == INVALID_SOCKET) {
        std::cerr << "Socket creation error & Error Code -> "
                  << getErrorCode(INVALID_SOCKET) << std::endl;
        return 1;
    }

    // Нужно связать сокет со структурой адреса (IP-адресом и портом)
    // Выполняется обычно, когда нужно слушать входящие соединения на
    // определенном порту
    int iBind = bind(tcpSocketServer, servinfo->ai_addr, servinfo->ai_addrlen);
    if (iBind == SOCKET_ERROR) {
        std::cerr << "Binding failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return 1;
    }

    std::cout << "Binding Success\n";

    // Установка прослушивания сокета с числом возможных соединений во входящей
    // очереди в размере 10-ти
    int iListen = listen(tcpSocketServer, 10);
    if (iListen == SOCKET_ERROR) {
        std::cerr << "Listening failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return 1;
    }

    std::cout << "Listening ...\n";

    addrinfo addr_c;
    socklen_t addrLen = sizeof(addr_c);

    // Принять подключение клиента
    // Данный сокет работает с принятым соединением, а старый продолжает слушать
    // новые входящие соединения
    int tcpSocketNew = accept(tcpSocketServer,
                              reinterpret_cast<sockaddr *>(&addr_c), &addrLen);
    if (tcpSocketNew == INVALID_SOCKET) {
        std::cerr << "Socket creating failed & Error Code -> "
                  << getErrorCode(INVALID_SOCKET) << std::endl;

        return 1;
    }
    std::cout << "Accept\n";

    if (getpeername(tcpSocketNew, reinterpret_cast<sockaddr *>(&addr_c),
                    &addrLen) < 0) {
        std::cerr << "Error GetPeerName" << std::endl;
    }

    char *clientip = new char[20];
    // Получение IP адреса
    strcpy(clientip,
           inet_ntoa(reinterpret_cast<sockaddr_in *>(&addr_c)->sin_addr));
    std::cout << "Client connected - " << clientip << std::endl;

    while (true) {
        /*
        Для нормальной работы данного цикла (ранее он был предназначен для
        одного подключения к серверу и отправки сообщений на него с эхо ответом)
        нужно реализовать поток для подключаемых клиентов и обработчик отправки
        всем клиентам сообщения. Соответственно, пришло сообщение от клиента ->
        отправил всем остальным.
        */

        // Получить данные от клиента
        int iReceiveFrom = recv(tcpSocketNew, buffer, BUFFER_SIZE, 0);
        if (iReceiveFrom == SOCKET_ERROR) {
            std::cerr << "Receiving failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            return 1;
        } else if (iReceiveFrom == 0 ||
                   std::string(buffer) == "\\c connection") {
            std::cout << "Client disconnected and connection closed - "
                      << clientip << std::endl;
            break;
        }

        std::cout << clientip << " - " << buffer << std::endl;

        // Отправить данные, полученные от клиента
        int iSend = send(tcpSocketNew, buffer, BUFFER_SIZE, 0);
        if (iSend == SOCKET_ERROR) {
            std::cerr << "Sending failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            return 1;
        }

        std::cout << "Sending Success\n";
    }

    int iCloseSocketClient = closeSocket(tcpSocketNew);
    if (iCloseSocketClient == SOCKET_ERROR) {
        return 1;
    }
}